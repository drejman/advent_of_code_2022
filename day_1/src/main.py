from pathlib import Path
from argparse import ArgumentParser
from typing import List

from utils.path import convert_path


def main(file: Path, top: int):
    data = read_data_from_file(file=file)
    elves = split_data_into_elves(data=data)
    calories = [sum(elf) for elf in elves]
    calories.sort(reverse=True)
    return sum(calories[:top])


def read_data_from_file(file: Path) -> List[str]:
    with open(file, "r") as f:
        file_content = f.read()
    data = file_content.split("\n")
    return data


def split_data_into_elves(data: List[str]) -> List[List[int]]:
    elves = []
    while "" in data:
        index = data.index("")
        calories = list(map(int, data[0:index]))
        elves.append(calories)
        data = data[index+1:]
    else:
        if data:
            elves.append(data)
    return elves


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--file", help="Path to file with data for the puzzle")
    parser.add_argument("--top", help="How many elves should be added", default=3)
    args = parser.parse_args()
    file = convert_path(args.file)
    top = int(args.top)
    print(main(file=file, top=top))
