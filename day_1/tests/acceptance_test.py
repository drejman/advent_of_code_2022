from pathlib import Path

from day_1 import main


test_filepath = r".\data\test.txt"


def test_with_test_data():
    assert main(Path(test_filepath), top=1) == 24000


def test_with_test_data_two_elves():
    assert main(Path(test_filepath), top=2) == 35000
