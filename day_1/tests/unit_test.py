from pathlib import Path

from day_1 import read_data_from_file, split_data_into_elves
from utils.path import convert_path


def test_convert_to_path_with_relative_path():
    relative_path = "data/test.txt"
    path_object = Path.cwd().joinpath(relative_path)
    assert convert_path(filepath=relative_path) == path_object


def test_convert_to_path_with_absolute_path():
    absolute_path = r"C:\python_learning\advent_of_code_2022\day_1\data\test.txt"
    path_object = Path.cwd().joinpath(absolute_path)
    assert convert_path(filepath=absolute_path) == path_object


def test_read_data_from_file():
    relative_path = "data/test.txt"
    path = Path.cwd().joinpath(relative_path)
    assert read_data_from_file(path) == ["1000", "2000", "3000", "", "4000", "", "5000", "6000", "", "7000", "8000",
                                         "9000", "", "10000", ""]


def test_split_data_into_elves():
    data = ["1", "2", "", "3", ""]
    assert split_data_into_elves(data=data) == [[1, 2], [3]]
