from pathlib import Path
from typing import List
from argparse import ArgumentParser

from utils.path import convert_path

from day_2.src.enums import Shape, Result

code = {"A": Shape.ROCK, "B": Shape.PAPER, "C": Shape.SCISSORS, "X": Result.LOSE, "Y": Result.DRAW, "Z": Result.WIN}
results_win = {Shape.ROCK: Shape.PAPER,
               Shape.SCISSORS: Shape.ROCK,
               Shape.PAPER: Shape.SCISSORS}
mapping = {(Result.WIN, k): v for k, v in results_win.items()}
mapping.update({(Result.LOSE, v): k for k, v in results_win.items()})


def read_data_from_file(file: Path) -> List[List[str]]:
    with open(file, "r") as f:
        file_content = f.read()
    data = [line.split(" ") for line in file_content.split("\n") if line != ""]
    return data


def get_single_result(opponent: Shape, you: Shape) -> Result:
    if opponent == you:
        return Result.DRAW
    else:
        if (opponent == Shape.ROCK and you == Shape.PAPER) or (opponent == Shape.PAPER and you == Shape.SCISSORS) or (
                opponent == Shape.SCISSORS and you == Shape.ROCK):
            return Result.WIN
        else:
            return Result.LOSE


def get_other_shape(opponent: Shape, result: Result) -> Shape:
    if result == Result.DRAW:
        return opponent
    else:
        return mapping.get((result, opponent))


def calculate_result(data: List[List[str]]) -> int:
    result = 0
    for line in data:
        opponent = code.get(line[0])
        game_result = code.get(line[1])
        # used in previous solution:
        # single_result = get_single_result(opponent=opponent, you=you)
        result += game_result.value
        result += get_other_shape(opponent=opponent, result=game_result).value
    return result


def main(file: Path) -> int:
    data = read_data_from_file(file=file)
    return calculate_result(data=data)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--file", help="Path to file with data for the puzzle")
    args = parser.parse_args()
    file = convert_path(args.file)
    print(main(file=file))
