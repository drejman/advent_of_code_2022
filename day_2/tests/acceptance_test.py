from pathlib import Path

from day_2 import main

test_filepath = r".\data\test.txt"


def test_data():
    assert main(Path(test_filepath)) == 12
