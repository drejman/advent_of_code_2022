from argparse import ArgumentParser
from pathlib import Path
from string import ascii_letters
from typing import Tuple, List
from itertools import islice

from utils.path import convert_path


priorities = {k: i for i, k in enumerate(ascii_letters, start=1)}


def read_data_from_file(file: Path) -> List[str]:
    with open(file, "r") as f:
        file_content = f.read().splitlines()
    return file_content


def get_total_priority(data: List[str], groupby=3) -> int:
    priority = 0
    assert len(data) % groupby == 0
    iterables = [list(islice(data, i, None, groupby)) for i in range(0, groupby)]
    for words in zip(*iterables):
        # compartments = split_into_halves(rucksack)
        duplicate = find_duplicate(words=words)
        priority += get_priority(duplicate)
    return priority


def split_into_halves(word: str) -> Tuple[str, str]:
    length = len(word)
    assert length % 2 == 0
    halflength = length // 2
    return word[0:halflength], word[halflength:]


def find_duplicate(words: List[str]) -> str:
    duplicate = set(words[0])
    for word in words[1:]:
        duplicate = duplicate.intersection(word)
    assert len(duplicate) == 1
    return duplicate.pop()


def get_priority(item: str) -> int:
    return priorities.get(item)


def main(file: Path) -> int:
    data = read_data_from_file(file=file)
    return get_total_priority(data=data)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--file", help="Path to file with data for the puzzle")
    args = parser.parse_args()
    file = convert_path(args.file)
    print(main(file=file))
