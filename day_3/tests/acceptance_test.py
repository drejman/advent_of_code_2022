from pathlib import Path

from day_3 import main


example_data = r".\data\test.txt"


def test_example_data():
    assert main(Path(example_data)) == 70
