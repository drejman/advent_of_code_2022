from pytest import mark

from day_3.src.main import split_into_halves, find_duplicate, get_priority


def test_split_into_halves():
    result = split_into_halves("vJrwpWtwJgWrhcsFMMfFFhFp")
    assert result[0] == "vJrwpWtwJgWr"
    assert result[1] == "hcsFMMfFFhFp"


def test_find_duplicate():
    assert find_duplicate(["vJrwpWtwJgWr", "hcsFMMfFFhFp"]) == "p"


@mark.parametrize(argnames="item, priority",
                  argvalues=[("p", 16), ("L", 38), ("P", 42), ("v", 22), ("t", 20), ("s", 19)])
def test_get_priority(item, priority):
    assert get_priority(item=item) == priority
