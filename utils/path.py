from pathlib import Path


def convert_path(filepath: str) -> Path:
    path = Path(filepath)
    if not path.is_absolute():
        path = Path.cwd().joinpath(filepath)
    assert path.is_file()
    return path
